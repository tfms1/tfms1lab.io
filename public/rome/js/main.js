const famID = document.getElementById("familyLife");
const recID = document.getElementById("recreation");

function showFam() {
    recID.style.display = "none";
    famID.style.display = "block";
}

function showRec() {
    famID.style.display = "none";
    recID.style.display = "block";
}